<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>


<link href="<?php echo base_url('/assets/js/dashboard.js')?>" rel="stylesheet" id="bootstrap-css">
<link href="<?php echo base_url('/assets/css/dashboard.css')?>" rel="stylesheet" id="bootstrap-css">
<!------ Include the above in your HEAD tag ---------->

<div id="throbber" style="display:none; min-height:120px;"></div>
<div id="noty-holder"></div>
<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="https://bryanrojasq.wordpress.com">
                <img src="http://placehold.it/200x50&text=LOGO" alt="LOGO">
            </a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li><a href="#" data-placement="bottom" data-toggle="tooltip" href="#" data-original-title="Stats"><i class="fa fa-bar-chart-o"></i>
                </a>
            </li>            
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?=$this->session->userdata('firstname').' '.$this->session->userdata('lastname')?> <b class="fa fa-angle-down"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="<?php  echo base_url('dashboard/profile');?>"><i class="fa fa-fw fa-user"></i> Edit Profile</a></li>
                    
                    
                    <li><a href="<?php  echo base_url('auth/logout');?>"><i class="fa fa-fw fa-power-off"></i> Logout</a></li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li>
                    <a href="#" data-toggle="collapse" data-target="#submenu-1"><i class="fa fa-fw fa-search"></i> MENU 1 <i class="fa fa-fw fa-angle-down pull-right"></i></a>
                    <ul id="submenu-1" class="collapse">
                        <li><a href="#"><i class="fa fa-angle-double-right"></i> SUBMENU 1.1</a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i> SUBMENU 1.2</a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i> SUBMENU 1.3</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" data-toggle="collapse" data-target="#submenu-2"><i class="fa fa-fw fa-star"></i>  MENU 2 <i class="fa fa-fw fa-angle-down pull-right"></i></a>
                    <ul id="submenu-2" class="collapse">
                        <li><a href="#"><i class="fa fa-angle-double-right"></i> SUBMENU 2.1</a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i> SUBMENU 2.2</a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i> SUBMENU 2.3</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-fw fa-user-plus"></i>  MENU 3</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-fw fa-paper-plane-o"></i> MENU 4</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-fw fa fa-question-circle"></i> MENU 5</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">
        <div class="container-fluid">
            <!-- Page Heading -->
            <div class="row" id="main" >
                <div class="col-sm-12 col-md-12 alert" id="content">
                    <h6>Welcome to Dashboard! Mr. <?=$this->session->userdata('firstname').' '.$this->session->userdata('lastname')?></h6>
                </div>
            </div>
            
            <!-- /.row -->
            
        </div>

        <div class="container">
            <?php 
                    if ( ! empty( $this->session->tempdata( 'update_msg' ) ) ) {
            ?>
                <p class="alert-success" align='center'><?php echo $this->session->tempdata( 'update_msg' ) ?></p>
            <?php
                    }
            ?>
            <h4>Profile Details</h4>
            <form id="login-form" class="form" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="email">First Name:</label>
                    <input name="firstname" class="form-control" placeholder="First Name" type="text" value='<?php echo $profileData['first_name']?>'>
                    <?php echo form_error('firstname', '<div class="error" >', '</div>'); ?>
                </div>
                <div class="form-group">
                    <label for="email">Last Name:</label>
                    <input name="lastname" class="form-control" placeholder="Last Name" type="text" value='<?php echo $profileData['last_name']?>'>
                     <?php echo form_error('lastname', '<div class="error" >', '</div>'); ?>
                </div>

                <div class="form-group">
                    <label for="email">Email Address:</label>
                    <input name="emailaddress" class="form-control" placeholder="Email Address" type="text" value='<?php echo $profileData['email_id']?>'>
                     <?php echo form_error('emailaddress', '<div class="error" >', '</div>'); ?>
                </div>

                <div class="form-group">
                    <label for="email">Profile Picture:</label>
                    <input name="profilePics" class="form-control" placeholder="Profile Picture" type="file" >
                    <input name="oldPics" type="hidden" value='<?php echo $profileData['image_name']?>'>
                    <?php echo form_error('profilePics', '<div class="error" >', '</div>'); ?>
                    <?php 
                            if ( ! empty( $this->session->tempdata( 'uploadError' ) ) ) {
                    ?>
                        <h6 class="error"><?php echo $this->session->tempdata( 'uploadError' )['error']; ?></h6>
                    <?php
                            }
                    ?>
                    <?php
                        if( !empty($profileData['image_name'])){
                    ?>
                            <p><a href='<?php echo base_url('assets/profile_image/'.$profileData['image_name'])?>' target='_blank'>download</a></p>
                    <?php
                        }
                    ?>
                   
                </div>

                <div class="form-group">
                    <label for="pwd">Department:</label>
                    <select class="form-control" id="relation" name='department' onchange="getSubDepartment(this.value)">
                        <option value=''>Select an option</option>
                        <?php
                            foreach ($department as $key => $value) {
                        ?>
                            <option value='<?php echo $value['dept_id']?>' <?=($profileData['department_id'] == $value['dept_id'] )?'selected':'' ?> ><?php echo $value['department_name']?></option>
                        <?php
                            }
                        ?>
                       
                    </select>
                    <?php echo form_error('department', '<div class="error" >', '</div>'); ?>
                </div>
                <div class="form-group">
                    <label for="pwd">Sub Department:</label>
                   
                    <select class="form-control" id="relation" name='subDepartment'>
                        <option value=''>Select an option</option>
                        <?php
                            foreach ($subdepartment as $key => $value) {
                        ?>
                            <option value='<?php echo $value['dept_id']?>'  <?=($profileData['subdepartment_id'] == $value['dept_id'] )?'selected':'' ?> ><?php echo $value['department_name']?></option>
                        <?php
                            }
                        ?>
                        
                    </select>
                    <?php echo form_error('subDepartment', '<div class="error" >', '</div>'); ?>
                </div>

                <div class="checkbox">
                
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
       
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
</div><!-- /#wrapper -->
<script>
    function getSubDepartment(department_id){
        if(department_id) {
                $.ajax({
                    url: "<?php echo base_url();?>/dashboard/ajax/",
                    type: "POST",
                    data: {department_id: department_id},
                    dataType : 'html',
                    success:function(data) {
                        $('select[name="subDepartment"]').empty();
                        $('select[name="subDepartment"]').append(data);
                       
                    }
                });
            }else{
                $('select[name="subDepartment"]').empty();
            }
    }
</script>