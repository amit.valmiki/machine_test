<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
<link href="<?php echo base_url('/assets/css/signup.css')?>" rel="stylesheet" id="bootstrap-css">
<style>
    .input-group>.form-control {
        width: unset;
    }
</style>
<div class="container">

<br>





<div class="card bg-light">
<article class="card-body mx-auto" style="max-width: 400px;">
	<h4 class="card-title mt-3 text-center text-info">Create Account</h4>
	<p class="text-center text-info">Get started with your account creation</p>
	
	
    <form id="login-form" class="form" action="<?php echo $action; ?>" method="post">
	<div class="form-group input-group">
		
        <input name="firstname" class="form-control" placeholder="First Name" type="text" value='Naveen' readonly>
       
        <?php echo form_error('firstname', '<span class="error" >', '</span>'); ?>
    </div> <!-- form-group// -->
    <div class="form-group input-group">
		
        <input name="lastname" class="form-control" placeholder="Last Name" type="text" value='Singh' readonly>
        <?php echo form_error('lastname', '<div class="error" >', '</div>'); ?>
    </div>
    <div class="form-group input-group">
    	
        <input name="emailaddress" class="form-control" placeholder="Email address" type="email" value='testing@yahoo.com' readonly>
        <?php echo form_error('emailaddress', '<div class="error" >', '</div>'); ?>
    </div> <!-- form-group// -->

    <div class="form-group input-group">
    	
        <input class="form-control" placeholder="Password" type="password" name='password'>
        <?php echo form_error('password', '<div class="error" >', '</div>'); ?>
    </div> <!-- form-group// -->  

    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-block"> Create Account  </button>
    </div> <!-- form-group// -->  

    <p class="text-center text-info">Have an account? <a href="<?php echo base_url('auth')?>">Log In</a> </p>                                                                 
</form>
</article>
</div> <!-- card.// -->

</div> 
<!--container end.//-->

<br><br>
