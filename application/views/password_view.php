<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link href="<?php echo base_url('/assets/css/login.css')?>" rel="stylesheet" id="bootstrap-css">
<!------ Include the above in your HEAD tag ---------->

<body>
    <div id="login">
        
        <div class="container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <form id="login-form" class="form" action="<?php echo $action; ?>" method="post">
                            <h3 class="text-center text-info">Login Screen</h3>
                            <!-- <div class="form-group">
                                <label for="username" class="text-info">Email Address:</label><br>
                                <input type="text" name="email_id" id="email_id" class="form-control">
                                <?//php echo form_error('email_id', '<div class="error" >', '</div>'); ?>
                            </div> -->
                            
                            <div class="form-group">
                                <label for="password" class="text-info">Password:</label><br>
                                <input type="password" name="password" id="password" class="form-control">
                                <?php echo form_error('password', '<div class="error" >', '</div>'); ?>
                            </div>
                            <?php 
                                  if ( ! empty( $this->session->tempdata( 'passwordPage' ) ) ) {
                            ?>
                                <h6 class="alert alert-danger text-center"><?php echo $this->session->tempdata( 'passwordPage' ); ?></h6>
                            <?php
                                  }
                            ?>
                            <div class="form-group">
                                <!-- <label for="remember-me" class="text-info"><span>Remember me</span> <span><input id="remember-me" name="remember-me" type="checkbox"></span></label><br> -->
								<br>
								<input type="submit" name="submit" class="btn btn-info btn-md" value="Submit">
                            </div>
                            <div id="register-link" class="text-right">
                                <a href="<?php echo base_url('signup')?>" class="text-info">Register here</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
