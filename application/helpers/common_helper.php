<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

if (!function_exists('generateSignature')) {

    function generateSignature($parameter, $key)
    {
        $signature = hash_hmac('sha512', $parameter, $key);
        return $signature;
    }
}

if (!function_exists('generateSalt')) {

    function generateSalt($length = 10)
    {
        $characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString     = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
if (!function_exists('otp')) {
    function otp($length = 10)
    {
        $characters       = '0123456789';
        $charactersLength = strlen($characters);
        $randomString     = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
if (!function_exists('time_elapsed_string')) {
    function time_elapsed_string($datetime, $full = false)
    {
        $now  = new DateTime;
        $ago  = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) {
            $string = array_slice($string, 0, 1);
        }

        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
}

if(!function_exists('getAllList')){
    function getAllList($tableName,$filter = null ,$joins = null, $groupBy = null )
    {
        $ci = &get_instance();
        $getAllList = $ci->Common_model->getAllList($tableName,$filter,$joins,$groupBy); 
        return $getAllList;
    }
}








if(!function_exists('insert')){
    function insert($tableName,$data)
    {
        $ci = &get_instance();
        $insert = $ci->Common_model->insert($tableName,$data); 
        return $insert;
    }
}
if(!function_exists('insertBatch')){
    function insertBatch($tableName,$data)
    {
        $ci = &get_instance();
        $insertBatch = $ci->Common_model->insertBatch($tableName,$data); 
        return $insertBatch;
    }
}
if(!function_exists('getDetail')){
    function getDetail($tableName,$filter = null)
    {
        $ci = &get_instance();
        $getDetail = $ci->Common_model->getDetail($tableName,$filter); 
        return $getDetail;
    }
}
if(!function_exists('update')){
    function update($tableName,$whereData,$updateData)
    {
        $ci = &get_instance();
        $update = $ci->Common_model->update($tableName,$whereData,$updateData); 
        return $update;
    }
}




//     function is_logged_in() {
//     // Get current CodeIgniter instance
//     $CI =& get_instance();
//     // We need to use $CI->session instead of $this->session
//     $user = $CI->session->userdata('userId');
//     if (!isset($user)) { 
//         redirect("auth");
//     } else { 
//         return true; 
//     }
// }

if(!function_exists('query')){
    function query($query)
    {
        $ci = &get_instance();
        $query = $ci->Common_model->query($query); 
        return $query;
    }
}
