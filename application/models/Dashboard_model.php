<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Dashboard_model extends CI_Model
{
  public function getDepartment()
  {
    $this->db->select( '*' );
    $this->db->from( 'dept' );
    $this->db->where('parent_id', 0 ); 
    $this->db->where( 'status', 1 );
    $query = $this->db->get();
    return $query->result_array();
  }

  public function getSubDepartment()
  {
    $this->db->select( '*' );
    $this->db->from( 'dept' );
    $this->db->where('parent_id !=',  '' ); 
    $this->db->where( 'status', 1 );
    $query = $this->db->get();
    return $query->result_array();
  }
    
}
?>