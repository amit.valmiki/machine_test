<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Common_model extends CI_Model
{
  public function insert($tableName,$data)
  {
    $this->db->insert($tableName,$data);
    return  $this->db->insert_id();
  }
  public function insertBatch($tableName,$data)
  {
    $this->db->insert_batch($tableName,$data);
    return  $this->db->insert_id();
  }
  public function getDetail($tableName,$filter = null)
  {
    if($filter)
    {
      $this->db->where($filter);
    }
    $query = $this->db->get($tableName);
    return $query->row_array();
  }
  public function getAllList($tableName,$filter = null)
  {
    if($filter)
    {
      $this->db->where($filter);
    }
    $query = $this->db->get($tableName);
    return $query->result_array();
  }
  public function update($tableName,$whereData,$updateData)
  {
    $this->db->trans_start();
    $this->db->where($whereData);
    $query = $this->db->update($tableName,$updateData);
    $this->db->trans_complete();
    return $this->db->trans_status();
  }
  

  public function query($query)
  {
    if($query)
    {
      $queryResult = $this->db->query($query);
      $result = $queryResult->result_array();
      return $result;
    }
  }

}
?>