<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Author : Amit Valmiki
	 * 
	 */
	public function __construct()
	{
		parent::__construct();
		
		$user = $this->session->userdata('userId');
		if (!isset($user)) { 
			redirect("auth");
		} else { 
			return true; 
		}
	}

	public function profile(){
		$data['title'] = 'Welcome to Dashboard'; 
		$data['action'] = base_url( 'dashboard/profileValidate' );

		$data['department'] = $this->Dashboard_model->getDepartment();
		$data['subdepartment'] = $this->Dashboard_model->getSubDepartment();

		$filter = array('userId' => $this->session->userdata('userId'),
    						'status' => 1);
		$data['profileData'] = $this->Common_model->getDetail('user',$filter);
		
		$this->load->view('dashboard_view',$data );
	}
	public function ajax(){
		$filter = array('parent_id' => $this->input->post('department_id'),
    						'status' => 1);
		$data = $this->Common_model->getAllList('dept',$filter);
		$dropdown = '';
		foreach ($data as $key => $value) {
			$dropdown .= "<option value=".$value['dept_id'].">".$value['department_name']."</option>"; 
		}
		echo $dropdown;
	}
	public function profileValidate() {	
		
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
		$this->form_validation->set_message('is_unique', 'The %s already exists');
		$this->form_validation->set_rules('firstname', 'Employee Name', 'trim|required');
		$this->form_validation->set_rules('lastname', 'Number', 'trim|required');
		$this->form_validation->set_rules('emailaddress', 'Email', 'required|trim|valid_email');
		$this->form_validation->set_rules('department', 'Department', 'trim|required');
		$this->form_validation->set_rules('subDepartment', 'Sub Department', 'trim|required');
		
		if ($this->form_validation->run() == false) {
			$this->profile();
        } else {

			if (  !empty( $_FILES['profilePics']['name'])) {
				ini_set('memory_limit', '-1');
				$config['upload_path'] = FCPATH.'assets/profile_image';
				$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
				$config['max_size'] = 15000;
				

				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('profilePics')) {
					$error = array('error' => $this->upload->display_errors());
					$this->session->set_tempdata('uploadError',$error,2);
					$this->profile();
				} else {
					$data = $this->upload->data();
				}
			}
			if($data['file_name']){
				$filename = $data['file_name'];
			}else{
				$filename = $this->input->post('oldPics');
			}
			$whereData = array( 'userId' => $this->session->userdata('userId') );
				$dataToInsert = array(	'first_name' => $this->input->post('firstname'),
										'last_name' => $this->input->post('lastname'),
										'email_id' => $this->input->post('emailaddress'),
										'image_name' => $filename,
										'department_id' => $this->input->post('department'),
										'subdepartment_id' => $this->input->post('subDepartment'),
										'ip_address' => $_SERVER['REMOTE_ADDR'],
									);
				
				$update = update( 'user', $whereData, $dataToInsert );
				
				if( $update ) {
					$this->session->set_tempdata( 'update_msg', 'Profile Updated Successfully', 2 );
					redirect('dashboard/profile' );
				} else {
					$this->session->set_tempdata( 'update_msg', ' Server Error, Please try again later ', 2 );
					redirect('dashboard/profile' );
				}
        	
        }
	}
	
}
