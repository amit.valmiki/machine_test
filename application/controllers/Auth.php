<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	/**
	 * Author : Amit Valmiki
	 * 
	 */
	public function __construct()
	{
		parent::__construct();
		
	}
	public function index(){
		$data['title'] = 'Login'; 
		$data['action'] = base_url( 'auth/validateUsername' );
		$this->load->view('username_view',$data );
	}
	public function validateUsername(){
		// Validation Errors
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
		$this->form_validation->set_rules('email_id', 'Email Address', 'trim|required');
		if ($this->form_validation->run() == false) {
			$this->index();
        } else {
        	$filter = array('email_id' => $this->input->post('email_id'),
    						'status' => 1);
			$userData = $this->Auth_model->userExist($filter);
			
        	if($userData)
        	{
        		$user_session_data = array(
			            'userId'  => $userData->userId,
						'username'     => $userData->email_id,
						'firstname'     => $userData->first_name,
						'lastname'     => $userData->last_name,
						);
						
        		$this->session->set_userdata($user_session_data);
        		redirect('auth/passwordScreen');
        	}else{
        		$this->session->set_tempdata('usernamePage',' User Does Not Exist',2);
        		redirect('./auth/');
        	}
        }
	}

	public function passwordScreen(){
		$data['title'] = 'Password'; 
		$data['action'] = base_url( 'auth/validatePassword' );
		$this->load->view('password_view',$data );
	}

	public function validatePassword(){
		// Validation Errors
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		if ($this->form_validation->run() == false) {
			$this->passwordScreen();
        } else {
        	$filter = array('email_id' => $this->session->userdata('username'),
    						'status' => 1);
			$passwordData = $this->Auth_model->passwordExist($filter);
			if($passwordData)
        	{
        		$encryptedPassword = generateSignature($this->input->post('password'),$passwordData->salt);
        		
        		if($encryptedPassword == $passwordData->password)
        		{
        			
        			redirect('dashboard/profile');
        		}else{
        			$this->session->set_tempdata('passwordPage',' Invalid Password',2);
        			redirect('./auth/passwordScreen');
				}
				
        	}else{
        		$this->session->set_tempdata('passwordPage', 'Enter Password is wrong',2);
        		redirect('./auth/passwordScreen');
        	}
        }
	}
	public function logout()
	{
		$this->session->sess_destroy();
        redirect('./auth');
	}
}
