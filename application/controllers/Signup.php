<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller {

	/**
	 * Author : Amit Valmiki
	 * 
	 */
	public function __construct(){
		parent::__construct();
        
	}
	public function index(){
		$data['title'] = 'Signup Form'; 
		$data['action'] = base_url( 'signup/register' );
		$this->load->view('signup_view',$data );
    }
    public function register() {	
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
		$this->form_validation->set_message('is_unique', 'The %s already exists');
		$this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
		$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('emailaddress', 'Email', 'required|trim|valid_email|is_unique[user.email_id]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		if ($this->form_validation->run() == false) {
			$this->index();
        }else{
        	$salt = generateSalt();
        	$password = generateSignature( $this->input->post('password'), $salt );
        	
        	$dataToInsert = array(	'first_name' => $this->input->post('firstname'),
                                    'last_name' => $this->input->post('lastname'),
                                    'email_id' => $this->input->post('emailaddress'),
    								'password' => $password,
                                    'salt' => $salt,
                                    'status' => 1,
    							);
        	$insert = insert( 'user', $dataToInsert );
        	if( $insert ) {
        		$this->session->set_tempdata( 'usernamePage', 'You have been successfully registered', 2 );
				redirect('auth' );
        	} else {
        		$this->session->set_tempdata( 'actionMessage', ' Server Error, Please try again later ', 2 );
    			redirect( 'signup' );
        	}
        }
	}
}